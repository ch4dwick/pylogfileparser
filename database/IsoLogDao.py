from sqlalchemy import Table, MetaData, Column, Integer, String, create_engine, engine, select
import logging

logger = logging.getLogger()
# These variables should only be declared once for this module
meta = MetaData()
py_isologdata = Table('py_isologdata', meta,
                Column('requestid', Integer, primary_key=True, autoincrement=False),
                Column('servername', String(12), primary_key=True, autoincrement=False),
                Column('ordertimestamp', Integer),
                Column('reportname', String(50)),
                Column('matchtype', String(13)),
                Column('fileno', String(12)),
                Column('riskid', String(12)),
                Column('address1', String(50)),
                Column('altaddress1', String(50)),
                Column('altaddress2', String(50)),
                Column('altaddress3', String(50)),
                Column('community', String(50)),
                Column('state', String(20)),
                Column('zip', String(5)),
                Column('county', String(25)),
                Column('filename', String(255)))

class IsoLogDao():
    engine = None
    dbUrl = None
    connection = None
    engine = None

    def __init__(self):
        self.dbUrl = engine.url.URL('mssql+pyodbc',
            host='TESTCID01',
            port='1433',
            database='TestSpecificRates',
            query={'driver':'SQL Server','Trusted_Connection':'yes'}
        )
        self.engine = create_engine(self.dbUrl, pool_size=10, echo=False)

    def save(self, dataList):
        if (len(dataList) > 0):
            self.connection = self.engine.connect()
            self.connection.execution_options(autocommit=True)
            #result = self.connection.execute(py_isologdata.insert(), dataList)
            for data in dataList:
                try:
                    result = self.connection.execute(py_isologdata.insert(), data)
                except Exception as exc:
                    logger.error('Problem with row: %s', exc.args)
                    pass
            """for row in dataList:
                print("row" + str(row))
                insert = self.py_isologdata.insert().values(row)
                result = self.connection.execute(insert)
                #print(result)"""
        else:
            return

    def list(self):
        s = select([py_isologdata]).limit(10)
        result = self.connection.execute(s)
        for row in result:
            print(row)
