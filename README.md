# PyLogFileParser #

A log file parsing script/service that searches/mines log files for a specific pattern. 

This was [ported from a Java project](https://bitbucket.org/ch4dwick/logfileparser) I made that was required to look in a bunch of log files for a specific log4j pattern. One of the reasons for this script's complexity was because some log entries do not exist on one line and therefore was considered a multi-line log entry.

[Log4jFileParser.py's](parsers/Log4jFileParser.py) algorithm was based off of [Otros Log Viewer](https://github.com/otros-systems/otroslogviewer) but in a more simplified variation since I really didn't need all those multiple objects to serve my needs. The string I was looking for in this case was an XML chunk which needed to be parsed first.

For efficiency, I roughly made the script spawn a new thread each time a specific file name pattern was detected so it can asynchronously look for another string while an existing one was still active.