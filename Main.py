import sys, os, fnmatch, threading, logging, logging.config
from parsers.Log4jFileParser import Log4jFileParser
from datetime import datetime,timedelta
from database.IsoLogDao import IsoLogDao

# logging settings
logging.config.fileConfig('logging.conf')

# create logger
logger = logging.getLogger(__name__)

# App behavior settings
logFilePrefix = 'SPILookup.log.'
logFolder = 'P:\\log'
batchThreshold = 25 # higher number means less threads created.
totalThreadsTime = timedelta(0)
threadsTime = []

class ProcessThread(threading.Thread):
	serverName = ""
	files = []
	dao = ""
	def __init__(self, filesFound, serverName, threadNo):
		threading.Thread.__init__(self)
		#self.name = serverName + str(threadNo)
		self.serverName = serverName
		self.counter = threadNo
		self.files = filesFound

	def run(self):
		global threadsTime
		global totalThreadsTime
		tTime = datetime.now()
		dataList = []
		logger.info('Entering %s', self.name)
		dao = IsoLogDao()

		for fp in self.files:
			dataList = processLog(fp, self.serverName)
			fp.close()
			dao.save(dataList)

		dao.connection.close()
		logger.info('Exiting %s', self.name)
		endTime = datetime.now() - tTime
		logger.info("{} time: {}".format(self.name, endTime))
		threadsTime.append(endTime)
		totalThreadsTime = totalThreadsTime + endTime
	



 # Get the logs in a specified folder with the declared date range and pattern.
 # @param startDate - Start of file name search range.
 # @param endDate - end of file name search range.
 # @return returns true if the process succeeded successfully.
def getLogs(startDate, endDate):
	"""Get the logs in a specified folder with the declared date range and pattern."""
	logger.info('Getting Files')
	files = []
	try:
		startDateObj = datetime.strptime(startDate, '%Y-%m-%d')
		endDateObj = datetime.strptime(endDate, '%Y-%m-%d')
	except ValueError:
		logger.error('Invalid date format or values')
		logger.error(sys.exc_info())
		return []

	if (startDateObj <= endDateObj):
		logDir = os.listdir(logFolder)
		# +1 is needed for a single day loop
		for date in range(int((endDateObj-startDateObj).days)+1):
			currentDate = startDateObj + timedelta(date)
			currentfilename = logFilePrefix + currentDate.strftime('%Y-%m-%d')
			
			filesFound = fnmatch.filter(logDir,currentfilename + "*")
			if (len(filesFound) > 0):
				# Store all file handles found for later processing.
				for fileName in filesFound:
					files.append(open(logFolder + "\\" + fileName,'r'))
	else:
		print('Start date is greater than end date')
	return files

# Process a file by extracting the lines we need.
def processLog(inputFile, serverName):
	parser = Log4jFileParser(inputFile)
	parser.serverName = serverName
	parser.parseInput()
	return parser.isoDataList

def main(args):
	if (len(args) < 4):
		print('Parameters incomplete. Usage: SPIParser start-date end-date server-name. Format yyyy-mm-dd serverName.')
	else:
		startDate = args[1]
		endDate = args[2]
		serverName = args[3]

		filesFound = getLogs(startDate, endDate)
		fileCount = 0
		threadNo = 1

		if(len(filesFound) > 0):
			files = []
			threads = []
			
			# Thread logic
			for fp in filesFound:
				files.append(fp)
				fileCount += 1
				totalThreadsTime = timedelta(0)
				""" Dynamically create threads based on the batch threshhold. Also round down the remaining files if it doesn't
				reach the minimum threshhold so it gets included in a thread of its own and doesn't get left out. """
				if(len(files) == batchThreshold or ((fileCount == len(filesFound) and len(files) < batchThreshold))):
					thread = ProcessThread(files, serverName, threadNo)
					thread.start()
					threads.append(thread)
					files = []
					threadNo += 1

			for t in threads:
				t.join()

			logger.info('Spawned threads: {}'.format(threadNo))
			logger.info('Total threads time: {}'.format(sum(threadsTime,timedelta())))
			if (threadNo == 0):
				threadNo = 1
			logger.info('Average processing time of all thread: {}'.format((sum(threadsTime, timedelta()) / len(threadsTime))))
startTime = datetime.now()
main(sys.argv)
logger.info("Script time: {}".format(datetime.now() - startTime))
logging.shutdown()