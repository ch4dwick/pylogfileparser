import re, os, logging
from parsers.IsoXmlParser import IsoXmlParser

logger = logging.getLogger()

class Log4jFileParser():
	serverName = None
	isoDataList = []
	logData = []
	inputFile = None
	isoRegPattern = None
	isoRequestPattern = None

	def __init__(self, inputFile):
		self.inputFile = inputFile
		self.isoRegPattern = re.compile("[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3} [A-Z]* [0-9]* [a-zA-Z]* - ")
		self.isoRequestPattern = re.compile(".* RequestID: ([0-9]{6})  XML recieved: ([\S\s]*)")
	
	def parseInput(self):
		match = ''
		bufferLines = ''
		self.logData = []
		
		logger.info('Reading file... %s', self.inputFile.name)
		for line in self.inputFile:
			# Catch the first line in the log entry. And make sure it wasn't an entry 
			# previously. Otherwise, this is a new log entry.
			if re.match(self.isoRegPattern, line) is not None:
				self.logData.append(match + bufferLines)
				match = line
				bufferLines = ''

			else:
				# No match means this is a line below a log entry
				bufferLines += line
		else:
			# If there's still more left in the buffere after the loop this must be part of the last entry.
			if bufferLines != '':
				self.logData.append(match + bufferLines)
				bufferLines = ''
		return self.saveResult()


		
	def saveResult(self):
		match = None
		isoData = {}
		self.isoDataList = []
		xmlParser = IsoXmlParser()
		logger.info('Finding <ISO> entries...')
		logger.info('Valid entries in file: %s', len(self.logData))
		for i,entry in enumerate(self.logData):
			match = re.match(self.isoRequestPattern, entry)
			if  match is not None:
				if 'RISKID-EXACT' in entry or 'ADDRESS-EXACT' in entry:
					isoData = { 'requestid' : match.group(1) }
					xmlParser.xmlData = match.group(2)
					xmlParser.parseInput()

					isoData.update(xmlParser.logData)
					isoData.update({'servername': self.serverName})
					isoData.update({'filename': os.path.basename(self.inputFile.name)})
					self.isoDataList.append(isoData)
		logger.info('Relevant records found in file: %s', len(self.isoDataList))
		return True