from defusedxml.ElementTree import fromstring
from xml.etree.ElementTree import ParseError
from xml.parsers.expat import ExpatError
import logging

parselogger = logging.getLogger('XmlParseResults')
logger = logging.getLogger()

class IsoXmlParser():
	xmlData = ''
	logData = {}

	# all key names are lowercase to match the field names in the database table.
	xpaths = {'reportname' : './PassportSvcRs/Reports/Report/ReportName', 
			'ordertimestamp' : './PassportSvcRs/Reports/Report/OrderTimeStamp', 
			'matchtype' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/PRODHEADER/MatchType', 
			'fileno' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/HEADER/FILENO', 
			'riskid' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/HEADER/RISKID', 
			'address1' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/HEADER/ADDRESS1', 
			'community' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/HEADER/COMMUNITY', 
			'county' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/HEADER/COUNTY', 
			'state' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/HEADER/STATE',
			'zip' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/HEADER/ZIP', 
			'altaddress1' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/HEADER/ALTADDRESS1', 
			'altaddress2' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/HEADER/ALTADDRESS2', 
			'altaddress3' : './PassportSvcRs/Reports/Report/ReportData/ISPI/*/HEADER/ALTADDRESS3'}

	def __init__(self):
		#self.xmlData = xmlData
		#print('Instantiated IsoXmlParser')
		pass

	def parseInput(self):
		self.logData = {}
		root = fromstring(self.xmlData) #This is the root tag <ISO>

		try:
			#print(root.find('./PassportSvcRs/Reports/Report/ReportName').text)
			for k, v in self.xpaths.items():
				if (root.find(v) is not None):
					#print(k,v)
					self.logData.update({k:root.find(v).text})
				else:
					# We still need to do this so the dictionary is the same consistent length.
					self.logData.update({k:None})

		except AttributeError as e:
			logger.exception("AttributeError [%s] [%s, %s]", e, k, v)

		#print('log data',self.logData)
		parselogger.debug("Log data: %s", self.logData)